import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-view-taskevent',
  templateUrl: './view-taskevent.page.html',
  styleUrls: ['./view-taskevent.page.scss'],
})
export class ViewTaskeventPage implements OnInit {

  @Input() titulo : string;
  @Input() text : string;
  @Input() image : string;

  constructor(public viewCtrl: ModalController) { }

  ngOnInit() {
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

}
