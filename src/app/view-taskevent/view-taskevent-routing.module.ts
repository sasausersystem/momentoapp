import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewTaskeventPage } from './view-taskevent.page';

const routes: Routes = [
  {
    path: '',
    component: ViewTaskeventPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewTaskeventPageRoutingModule {}
