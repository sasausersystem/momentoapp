import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewTaskeventPage } from './view-taskevent.page';

describe('ViewTaskeventPage', () => {
  let component: ViewTaskeventPage;
  let fixture: ComponentFixture<ViewTaskeventPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTaskeventPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewTaskeventPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
