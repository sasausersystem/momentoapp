import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewTaskeventPageRoutingModule } from './view-taskevent-routing.module';

import { ViewTaskeventPage } from './view-taskevent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewTaskeventPageRoutingModule
  ],
  declarations: [ViewTaskeventPage]
})
export class ViewTaskeventPageModule {}
