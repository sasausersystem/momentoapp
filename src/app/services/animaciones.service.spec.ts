import { TestBed } from '@angular/core/testing';

import { AnimacionesService } from './animaciones.service';

describe('AnimacionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnimacionesService = TestBed.get(AnimacionesService);
    expect(service).toBeTruthy();
  });
});
