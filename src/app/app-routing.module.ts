import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'index',
    loadChildren: () => import('./index/index.module').then( m => m.IndexPageModule)
  },
  {
    path: 'evento/:employe/:ids',
    loadChildren: () => import('./detailevento/detailevento.module').then( m => m.DetaileventoPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./paginas/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'login/:token',
    loadChildren: () => import('./paginas/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'temas',
    loadChildren: () => import('./modals/temas/temas.module').then( m => m.TemasPageModule)
  },
  {
    path: 'view-taskevent',
    loadChildren: () => import('./view-taskevent/view-taskevent.module').then( m => m.ViewTaskeventPageModule)
  },
  {
    path: 'view-taskitinerario',
    loadChildren: () => import('./view-taskitinerario/view-taskitinerario.module').then( m => m.ViewTaskitinerarioPageModule)
  },
  {
    path: 'view-portfolio',
    loadChildren: () => import('./view-portfolio/view-portfolio.module').then( m => m.ViewPortfolioPageModule)
  },
  {
    path: 'view-portfolio-detail',
    loadChildren: () => import('./view-portfolio-detail/view-portfolio-detail.module').then( m => m.ViewPortfolioDetailPageModule)
  },
  {
    path: 'view-blog',
    loadChildren: () => import('./view-blog/view-blog.module').then( m => m.ViewBlogPageModule)
  },
  {
    path: 'view-blog-detail',
    loadChildren: () => import('./view-blog-detail/view-blog-detail.module').then( m => m.ViewBlogDetailPageModule)
  },
  {
    path: 'view-servicios',
    loadChildren: () => import('./view-servicios/view-servicios.module').then( m => m.ViewServiciosPageModule)
  },
  {
    path: 'view-servicios-detail',
    loadChildren: () => import('./view-servicios-detail/view-servicios-detail.module').then( m => m.ViewServiciosDetailPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
