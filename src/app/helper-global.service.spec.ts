import { TestBed } from '@angular/core/testing';

import { HelperGlobalService } from './helper-global.service';

describe('HelperGlobalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HelperGlobalService = TestBed.get(HelperGlobalService);
    expect(service).toBeTruthy();
  });
});
