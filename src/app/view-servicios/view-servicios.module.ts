import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewServiciosPageRoutingModule } from './view-servicios-routing.module';

import { ViewServiciosPage } from './view-servicios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewServiciosPageRoutingModule
  ],
  declarations: [ViewServiciosPage]
})
export class ViewServiciosPageModule {}
