import { Component, OnInit } from '@angular/core';
import { ApiRestService } from '../services/apirest.service';
import { ModalController, LoadingController } from '@ionic/angular';
import { ViewServiciosDetailPage } from '../view-servicios-detail/view-servicios-detail.page';
import { NavController, NavParams } from '@ionic/angular';
import { HelperGlobalService } from '../helper-global.service';
import { AnimacionesService } from '../services/animaciones.service';




@Component({
  selector: 'app-view-servicios',
  templateUrl: './view-servicios.page.html',
  styleUrls: ['./view-servicios.page.scss'],
})
export class ViewServiciosPage implements OnInit {

  articulos : any    = [];
  error     : number = 0;

  constructor(private SApi: ApiRestService, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private navCtrl: NavController, public helper: HelperGlobalService, public anima: AnimacionesService) { }

  ngOnInit() {
    this.presentLoader();
    this.SApi.SiteQueryServicios().subscribe( (articulos:any) => {
      if(articulos.error == null){

        this.error = 0;

        this.articulos = articulos.data;

      } else {
        this.error = 0;

        this.articulos = [];

      }
    } );
  }

  goToIndex(){
    this.anima.transitionWithSlide('right', 400);
    this.navCtrl.navigateRoot('/');
  }

  async openDetails(data:any){
    const modal = this.modalCtrl.create({
      component: ViewServiciosDetailPage,
      componentProps: data
    });

    return (await modal).present();
  }

  async presentLoader(){
    const loading = await this.loadingCtrl.create({
      message: 'Cargando',
      duration: 5000
    });
    await loading.present();

    await loading.dismiss();
  }

  doRefresh(event){

    setTimeout(() => {
      this.ngOnInit();
      event.target.complete();
    }, 2000);
  
   }

}
