import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AnimacionesService } from './services/animaciones.service';

@Injectable({
  providedIn: 'root'
})
export class HelperGlobalService {

  constructor( private navCtrl: NavController, private anima: AnimacionesService ) { }

  openExternal(url:string){
    window.open( url, '_blank' );
  }

  openInternal(url:string, dir:string, time: number){
    this.anima.transitionWithSlide(dir, time);
    this.navCtrl.navigateRoot(url);
  }
}
