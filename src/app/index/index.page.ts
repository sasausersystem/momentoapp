import { Component, NgZone, ChangeDetectorRef, OnInit } from '@angular/core';
import { AccesosService } from '../services/accesos.service';
import { ApiRestService } from '../services/apirest.service';
import { Router } from '@angular/router';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { NavController, NavParams } from '@ionic/angular';
import { LoginPage } from '../paginas/login/login.page';
import { ModalController } from '@ionic/angular';
import { HelperGlobalService } from '../helper-global.service';

import { AnimacionesService } from '../services/animaciones.service';

//MODALES PARA LA PARTE PUBLICA
import { ViewPortfolioPage } from '../view-portfolio/view-portfolio.page';


@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {

  reponse : any;

  employe = {
    email: '',
    token: ''
  };

  datosMomento : any = {
    'facebook': 'https://www.facebook.com/eventosmomentomx',
    'youtube' : 'https://www.youtube.com/channel/UCS2tDAP429LenqdSprPApmw',
    'instagram': 'https://www.instagram.com/eventosmomentomx/',
    'twitter' : 'https://twitter.com/eventomomentomx',
    'whatsapp': 'https://wa.me/+525541847954',
    'web'     : 'https://eventosmomento.com/'
  };

  isConnected : any;

  datosEmpleado: any = [];

  empresa     : any = Object;

  colors      : any = Object;

  eventos    : any = [];

  notificiaciones : any = [];

  itinhome : any = [];

  enlaceeventoitin : string;

  constructor(
    private Router: Router,
    private SAccesso: AccesosService, 
    private SApi: ApiRestService,
    private ChangeD: ChangeDetectorRef,
    private Zone: NgZone,
    private Noti: LocalNotifications,
    private sanitaizer: DomSanitizer,
    private navCtrl: NavController,
    public  modalCtrl: ModalController,
    public  helper: HelperGlobalService,
    private anima: AnimacionesService
  ) {
    this.SAccesso.comprobeLogin();
  }

  goToDetaildEvent(url:string){
    this.anima.transitionWithSlide('left', 400);
    this.navCtrl.navigateRoot(url);
  }

  goToPortafolio(){
    this.anima.transitionWithSlide('left', 400);
    this.navCtrl.navigateRoot('/view-portfolio');
  }

  goToBlog(){
    this.anima.transitionWithSlide('left', 400);
    this.navCtrl.navigateRoot('/view-blog');
  }

  goToServices(){
    this.anima.transitionWithSlide('left', 400);
    this.navCtrl.navigateRoot('/view-servicios');
  }

  goToLogin(){
    this.anima.transitionWithSlide('left', 400);
    this.navCtrl.navigateRoot('/login');
  }

  goToMomento(){
    this.anima.transitionWithSlide('left', 400);
    this.navCtrl.navigateRoot('/about');
  }

  ngOnInit(){    
    //this.sincronizar();
  }

  ionViewWillEnter(){
    this.sincronizar();
  }

  ionViewWillLeave(){
    this.SAccesso.comprobeLogin();
  }

  cerrar(){
    this.SAccesso.cerrarSession().then((done)=>{
      this.anima.transitionWithSlide('right', 400);
      this.navCtrl.navigateRoot('/index');
      this.sincronizar();
    });
  }

  async verifyToken(){

    this.SApi.tokenVerify( this.employe.token ).then( (done:any)=>{

      if(done.error == null){

        this.SAccesso.iniciarSession(done.data);
        this.datosEmpleado = this.SAccesso.obtenerDatos('empleadoConectado');
        this.anima.transitionWithSlide('left', 400);
        this.navCtrl.navigateRoot('/index');

      }

    }, (error:any)=>{

    } );
    
  }

  verifyTwoFactory(){
    this.SApi.EmpleadoQueryByEmail( this.employe.email ).then((done:any)=>{

      this.reponse = done;

      if( this.reponse.error == null ){

        this.ChangeD.markForCheck();

      } else {

      }
      
    }, (error)=>{

    });
  }

  redirectTo(uri:string){
    this.Router.navigated = false;
    this.Router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
    this.Router.navigate([uri]));
 }

 doRefresh(event){

  setTimeout(() => {
    this.sincronizar();
    event.target.complete();
  }, 2000);

 }

 sincronizar(){
  this.SAccesso.isConectado().then((done:any)=>{
    this.eventos = [];
    this.notificiaciones = [];
    this.isConnected = done;
    if(this.isConnected == true){
      this.SAccesso.obtenerDatos('empleadoConectado').then((done)=>{
        this.datosEmpleado = done;

        this.SApi.ItinerarioQueryByTime(parseInt(this.datosEmpleado.id)).subscribe( (done:any) => {

          if(  done.error == null  ){
            this.itinhome = done.data;
            if(done.data.length > 0){
              this.enlaceeventoitin = '/evento/'+this.datosEmpleado.id+'/'+done.data[0].ideven;
            }
            
          } else {
            this.itinhome = [];
          }

        } );
        
        //DATOS EVENTOS POR EMPRESA
        this.SApi.EventosQueryByEmpresa( parseInt(this.datosEmpleado.id) ).subscribe((done:any)=>{
          if(done.error == null){
            
            done.data.forEach((element:any) => {
              this.eventos.push({
                id: element.id,
                titulo: element.titulo,
                subtitulo: element.subtitulo,
                image: element.mainimage,
                enlace: '/evento/'+this.datosEmpleado.id+'/'+element.id
              });
            });

          } else {
            this.eventos = null;
          }
        });
                  
        //DATOS EMPRESA.....................................................
        this.SApi.EmpresaQueryById( parseInt(this.datosEmpleado.empresa) ).subscribe((done:any)=>{

          this.empresa ={
            nombre: done.data.nombre,
            descripcion: done.data.descripcion,
            facebook: this.sanitaizer.bypassSecurityTrustUrl('https://facebook.com/'+done.data.facebook),
            haveFb  : (done.data.facebook!="") ? true : false,
            twitter:  this.sanitaizer.bypassSecurityTrustUrl('https://twitter.com/'+done.data.twitter),
            haveTw  : (done.data.twitter!="") ? true : false,
            youtube:  this.sanitaizer.bypassSecurityTrustUrl('https://www.youtube.com/channel/'+done.data.youtube_channel),
            haveYt  : (done.data.youtube_channel!="") ? true : false,
            instagram:  this.sanitaizer.bypassSecurityTrustUrl('https://www.instagram.com/'+done.data.instagram),
            haveIg  : (done.data.instagram!="") ? true : false,
            web: this.sanitaizer.bypassSecurityTrustUrl('https://'+done.data.paginaweb),
            haveWb  : (done.data.paginaweb!="") ? true : false,
            telefono: done.data.telefono,
            direccion: done.data.direccion,
            colors: {
              primary: done.data.primary_color,
              background: done.data.background_color,
              schema: 'https://app.eventosmomento.com' + done.data.esquema
            },
            logo: done.data.Intro_imagen
          };

         

          this.colors.background = done.data.background_color;
          this.colors.primary    = done.data.primary_color;
          this.colors.schema     = 'https://app.eventosmomento.com' + done.data.esquema;          

        },(error:any)=>{

        });
      },(error)=>{

      });
    }
  });
 }

}
