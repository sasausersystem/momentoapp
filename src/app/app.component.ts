import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AccesosService } from './services/accesos.service';
import { ApiRestService } from './services/apirest.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Router } from '@angular/router';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { NgZone } from '@angular/core';
import { LoginPage } from './paginas/login/login.page';


declare var cordova: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  notificiaciones : any = [];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private SAccesso: AccesosService,
    private SApi: ApiRestService,
    private Noti: LocalNotifications,
    private Backgroundmode: BackgroundMode,
    private Route :Router,
    private deeplinks: Deeplinks,
    private zone: NgZone,
    private navCtrl: NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    console.log(this.platform.pause);
    this.platform.ready().then(() => {
      //this.Backgroundmode.enable();
      
      this.statusBar.styleBlackTranslucent();
      this.splashScreen.hide();

      setInterval( ()=>{
        this.refreshApp();
      }, 30000 );

      this.Noti.on('click').subscribe( (response) => {
        let data = response.data;
        this.Route.navigate(['evento/'+data.usuario+'/'+data.evento]);
      } );

      this.septuDeeplink();
    });
  }

  septuDeeplink(){
    this.deeplinks.route({ '/login/:token':  LoginPage}).subscribe(
      match => {
        console.log('Successfully matched route', match);
 
        this.zone.run(() => {
          this.Route.navigateByUrl(match.$link.path);
        });
      },
      nomatch => {
         // Run the navigation in the Angular zone
         this.zone.run(() => {
            this.navCtrl.navigateRoot('/index');
          });
      }
    );
  }


  refreshApp(){
    this.SAccesso.obtenerDatos('empleadoConectado').then( (done:any) => {

      if(done!=null){

        this.SApi.ItinerarioQueryByEmploye(  parseInt(done.id) ).subscribe( (data:any) => {
          let idusuario= parseInt(done.id);
          if(data.error == null){
            this.notificiaciones = [];
            let idn = 1;
            data.data.forEach((element:any) => {
              let fechaE = element.fecha.split('-');
              let nuevaF = fechaE[1] + '-' + fechaE[2] + '-' + fechaE[0] + ' ' + element.hora;
              let times  = (new Date( nuevaF ).getTime()) - 300000;
              this.notificiaciones.push({
                id: idn,
                title: element.evento,
                text: element.nombre+'\n'+element.descripcion,
                attachments: [element.evento_imagen],
                launch: ()=>{
                  this.navCtrl.navigateRoot(`/evento/${idusuario}/${element.ideven}`);
                },
                trigger: {
                    at:  new Date(times)
                },
                data: {
                  usuario: idusuario,
                  evento: element.ideven
                },
                foreground: true,
                vibrate: true,
                sound: 'file://assets/sound/notification.mp3',
                icon: element.evento_imagen,
                actions: [
                  { id: 'vervento',  title: 'VER', type: 'button' }
                ] 
              });
              idn++;
            });
            this.Noti.hasPermission().then((is:any)=>{
              if(done == true){
                this.Noti.schedule(this.notificiaciones);
              }
            });
            this.Noti.requestPermission().then((done:any) => {
              if(done == true){
                this.Noti.schedule(this.notificiaciones);
              }
            });
  
          } else {
  
          }
        } );

      } else {
      }

    } );
  }
}
