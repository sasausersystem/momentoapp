import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, Router } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { EmpleadosComponent } from './componentes/empleados/empleados.component';
import { EmpresaComponent } from './componentes/empresa/empresa.component';
import { EventosComponent } from './componentes/eventos/eventos.component';
import { ItinerarioComponent } from './componentes/itinerario/itinerario.component';

import { IonicStorageModule } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AccesosService } from './services/accesos.service';
import { ApiRestService } from './services/apirest.service';
import { HelperGlobalService } from './helper-global.service';

import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { NativePageTransitions } from '@ionic-native/native-page-transitions/ngx';

import { GalleryforgoogleModule } from './galleryforgoogle/galleryforgoogle.module';
import { ViewTaskeventPageModule } from './view-taskevent/view-taskevent.module';
import { ViewTaskitinerarioPageModule } from './view-taskitinerario/view-taskitinerario.module';
import { ViewPortfolioPageModule } from './view-portfolio/view-portfolio.module';
import { ViewPortfolioDetailPageModule } from './view-portfolio-detail/view-portfolio-detail.module';
import { ViewBlogDetailPageModule } from './view-blog-detail/view-blog-detail.module';

import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { ViewServiciosDetailPageModule } from './view-servicios-detail/view-servicios-detail.module';


@NgModule({
  declarations: [
    AppComponent,
    EmpleadosComponent,
    EmpresaComponent,
    EventosComponent,
    ItinerarioComponent
  ],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    GalleryforgoogleModule,
    ViewTaskeventPageModule,
    ViewTaskitinerarioPageModule,
    ViewPortfolioPageModule,
    ViewPortfolioDetailPageModule,
    ViewBlogDetailPageModule,
    ViewServiciosDetailPageModule
  ],
  exports: [
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AccesosService,
    ApiRestService,
    HelperGlobalService,
    LocalNotifications,
    NativePageTransitions,
    BackgroundMode,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Deeplinks
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(){
  }
}
