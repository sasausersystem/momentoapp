import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewTaskitinerarioPageRoutingModule } from './view-taskitinerario-routing.module';

import { ViewTaskitinerarioPage } from './view-taskitinerario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewTaskitinerarioPageRoutingModule
  ],
  declarations: [ViewTaskitinerarioPage]
})
export class ViewTaskitinerarioPageModule {}
