import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewTaskitinerarioPage } from './view-taskitinerario.page';

describe('ViewTaskitinerarioPage', () => {
  let component: ViewTaskitinerarioPage;
  let fixture: ComponentFixture<ViewTaskitinerarioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTaskitinerarioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewTaskitinerarioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
