import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewTaskitinerarioPage } from './view-taskitinerario.page';

const routes: Routes = [
  {
    path: '',
    component: ViewTaskitinerarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewTaskitinerarioPageRoutingModule {}
