import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-view-taskitinerario',
  templateUrl: './view-taskitinerario.page.html',
  styleUrls: ['./view-taskitinerario.page.scss'],
})
export class ViewTaskitinerarioPage implements OnInit {

  @Input() nombre       : string;
  @Input() fecha        : string;
  @Input() hora         : string;
  @Input() descripcion  : string;

  constructor(public viewCtrl: ModalController) { }

  ngOnInit() {
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

}
