import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewBlogDetailPageRoutingModule } from './view-blog-detail-routing.module';

import { ViewBlogDetailPage } from './view-blog-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewBlogDetailPageRoutingModule
  ],
  declarations: [ViewBlogDetailPage]
})
export class ViewBlogDetailPageModule {}
