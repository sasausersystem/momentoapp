import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewBlogDetailPage } from './view-blog-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ViewBlogDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewBlogDetailPageRoutingModule {}
