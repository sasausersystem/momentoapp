import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-view-blog-detail',
  templateUrl: './view-blog-detail.page.html',
  styleUrls: ['./view-blog-detail.page.scss'],
})
export class ViewBlogDetailPage implements OnInit {

  @Input() image : string;
  @Input() title : string;
  @Input() intro : any;
  @Input() link  : string;

  constructor(private ctrlView: ModalController, private sanitazer: DomSanitizer) { }

  ngOnInit() {
    var html = document.getElementById("introtext");
    this.intro = this.sanitazer.bypassSecurityTrustHtml(this.intro);
    this.intro = this.intro.changingThisBreaksApplicationSecurity;
    html.innerHTML = this.intro;
  }

  closeDetails() {
    this.ctrlView.dismiss();
  }

  openArticle(url:string){
    window.open(url, '_blank');
  }

}
