import { Component, OnInit } from '@angular/core';
import { ApiRestService } from '../services/apirest.service';
import { ModalController, LoadingController} from '@ionic/angular';
import { ViewBlogDetailPage } from '../view-blog-detail/view-blog-detail.page';
import { NavController, NavParams } from '@ionic/angular';
import { HelperGlobalService } from '../helper-global.service';
import { AnimacionesService } from '../services/animaciones.service';

@Component({
  selector: 'app-view-blog',
  templateUrl: './view-blog.page.html',
  styleUrls: ['./view-blog.page.scss'],
})
export class ViewBlogPage implements OnInit {

  articulos : any    = [];
  error     : number = 0;
  loader    : any = null;

  constructor(private SApi: ApiRestService, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private navCtrl: NavController, public helper: HelperGlobalService, private anima: AnimacionesService) {
  }

  ngOnInit() {

    this.presentLoader();

    this.SApi.SiteQueryBlog().subscribe( (articulos:any) => {
      
      if(articulos.error == null){

        this.error = 0;

        this.articulos = articulos.data;

        //this.loadingCtrl.dismiss();

      } else {
        this.error = 0;

        this.articulos = [];

      }
    } );
  }

  async openDetails(data:any){
    const modal = this.modalCtrl.create({
      component: ViewBlogDetailPage,
      componentProps: data
    });

    return (await modal).present();
  }

  async presentLoader(){
    const loading = await this.loadingCtrl.create({
      message: 'Cargando',
      duration: 5000
    });
    await loading.present();

    await loading.dismiss();
  }

  goToIndex(){
    this.anima.transitionWithSlide('right', 400);
    this.navCtrl.navigateRoot('/');
  }

  doRefresh(event){

    setTimeout(() => {
      this.ngOnInit();
      event.target.complete();
    }, 2000);
  
  }

}
