import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewServiciosDetailPage } from './view-servicios-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ViewServiciosDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewServiciosDetailPageRoutingModule {}
