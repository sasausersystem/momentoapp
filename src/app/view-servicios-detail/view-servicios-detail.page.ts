import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-view-servicios-detail',
  templateUrl: './view-servicios-detail.page.html',
  styleUrls: ['./view-servicios-detail.page.scss'],
})
export class ViewServiciosDetailPage implements OnInit {

  @Input() image : string;
  @Input() title : string;
  @Input() intro : any;
  @Input() link  : string;

  constructor(private ctrlView: ModalController, private sanitazer: DomSanitizer) { }

  ngOnInit() {
    var html = document.getElementById("introtext");
    this.intro = this.sanitazer.bypassSecurityTrustHtml(this.intro);
    this.intro = this.intro.changingThisBreaksApplicationSecurity;
    html.innerHTML = this.intro;
  }

  closeDetails() {
    this.ctrlView.dismiss();
  }

  openArticle(url:string){
    window.open(url, '_blank');
  }

}
