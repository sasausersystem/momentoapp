import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewServiciosDetailPageRoutingModule } from './view-servicios-detail-routing.module';

import { ViewServiciosDetailPage } from './view-servicios-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewServiciosDetailPageRoutingModule
  ],
  declarations: [ViewServiciosDetailPage]
})
export class ViewServiciosDetailPageModule {}
