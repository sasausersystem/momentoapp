import { Component, NgZone, ChangeDetectorRef, OnInit } from '@angular/core';
import { AccesosService } from '../services/accesos.service';
import { ApiRestService } from '../services/apirest.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  reponse : any;

  employe = {
    email: '',
    token: ''
  };

  isConnected : any;

  datosEmpleado: any = [];

  constructor(
    private Router: Router,
    private SAccesso: AccesosService, 
    private SApi: ApiRestService,
    private ChangeD: ChangeDetectorRef,
    private Zone: NgZone
  ) {
    this.datosEmpleado = this.SAccesso.obtenerDatos('empleadoConectado');
    this.SAccesso.isConectado().then((done:any)=>{
      this.isConnected = done;
    });
  }

  ngOnInit(){    

  }

  cerrar(){
    this.SAccesso.cerrarSession().then((done)=>{
      window.location.href = '/index';
    });
  }

  async verifyToken(){

    this.SApi.tokenVerify( this.employe.token ).then( (done:any)=>{

      if(done.error == null){

        this.SAccesso.iniciarSession(done.data);
        this.datosEmpleado = this.SAccesso.obtenerDatos('empleadoConectado');
        window.location.href = '/index';

      }

    }, (error:any)=>{

    } );
    
  }

  verifyTwoFactory(){
    this.SApi.EmpleadoQueryByEmail( this.employe.email ).then((done:any)=>{

      this.reponse = done;

      if( this.reponse.error == null ){

        this.ChangeD.markForCheck();

      } else {

      }
      
    }, (error)=>{
    });
  }

  redirectTo(uri:string){
    this.Router.navigated = false;
    this.Router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
    this.Router.navigate([uri]));
 }

}
