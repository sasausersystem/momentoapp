import { Component, OnInit } from '@angular/core';
import { AccesosService } from '../../services/accesos.service';
import { ApiRestService } from '../../services/apirest.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, NavParams } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { AnimacionesService } from '../../services/animaciones.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  employe = {
    email: '',
    token: ''
  };

  datosEmpleado: any = [];

  response : any = {};

  error : any;

  constructor(
    private Router: Router,
    private SAccesso: AccesosService, 
    private SApi: ApiRestService,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private anima: AnimacionesService
  ) { 
    this.error = 0;
    this.SAccesso.comprobeLogin();
  }

  ionViewWillEnter(){
    let token = this.route.snapshot.paramMap.get('token');
    if(token){
      this.employe.token = token;
      this.verifyToken();
    }
    
  }  

  ngOnInit() {
    
  }

  goToIndex(){
    this.anima.transitionWithSlide('right', 400);
    this.navCtrl.navigateRoot('/');
  }  

  verifyTwoFactory(){
    this.SApi.EmpleadoQueryByEmail( this.employe.email ).then((done:any)=>{

      this.response = done;
      this.error    = done.error;
      
    }, (error)=>{

      this.response = null;
      this.error    = 1;

    });
  }

  async verifyToken(){

    this.SApi.tokenVerify( this.employe.token ).then( (done:any)=>{

      if(done.error == null){

        this.SAccesso.iniciarSession(done.data);
        this.datosEmpleado = this.SAccesso.obtenerDatos('empleadoConectado');
        //this.Router.navigate(['/index']);
        this.anima.transitionWithSlide('left', 400);
        this.navCtrl.navigateRoot('/');

      }

    }, (error:any)=>{

    } );
    
  }

}
