import { Component, OnInit } from '@angular/core';
import { AccesosService } from '../services/accesos.service';
import { ApiRestService } from '../services/apirest.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';
import { ViewTaskeventPage } from '../view-taskevent/view-taskevent.page';
import { ViewTaskitinerarioPage } from '../view-taskitinerario/view-taskitinerario.page';
import { NavController, NavParams } from '@ionic/angular';
import { HelperGlobalService } from '../helper-global.service';

@Component({
  selector: 'app-detailevento',
  templateUrl: './detailevento.page.html',
  styleUrls: ['./detailevento.page.scss'],
})
export class DetaileventoPage implements OnInit {

  evento: any = {};
  bullets: any = {};
  video:any;
  ids       : any;
  employeId : any;
  verItin   : boolean;

  constructor(
    private SAccesso: AccesosService, 
    private SApi: ApiRestService,
    private ActiveRouted: ActivatedRoute,
    private sanitazer: DomSanitizer,
    public  modalCtrl: ModalController,
    private navCtrl: NavController,
    public helper: HelperGlobalService
  ) { 

  }

  goToIndex(){
    this.navCtrl.navigateRoot('/');
  }

  ngOnInit() {

    this.ids = this.ActiveRouted.snapshot.paramMap.get('ids');
    this.employeId = this.ActiveRouted.snapshot.paramMap.get('employe');

    this.SApi.EventoQueryById( parseInt( this.ids ) ).subscribe((done:any)=>{

      if( done.error == null ){
        let videoYT = 'https://www.youtube.com/embed/'+done.data.youtube+'?rel=0&controls=0&showinfo=0';
        let videoVM = 'https://player.vimeo.com/video/'+done.data.vimeo+'?color=ffffff&title=0&byline=0&portrait=0';
        let mapaG   = done.data.ubicacion;
        this.evento.titulo = done.data.titulo;
        this.evento.subtitulo = done.data.subtitulo;
        this.evento.texto = done.data.texto;
        this.evento.mainImage = done.data.intro_img;
        this.evento.secondImg = done.data.imagen2;
        this.evento.fecha     = done.data.fecha;
        this.evento.provVideo = (done.data.youtube != "") ? 'YT' : 'VM';
        this.evento.youtube   = this.sanitazer.bypassSecurityTrustResourceUrl(videoYT);
        this.evento.vimeo     = this.sanitazer.bypassSecurityTrustResourceUrl(videoVM);
        this.evento.powerwords= done.data.palabraspoder;
        this.evento.url       = this.sanitazer.bypassSecurityTrustResourceUrl( done.data.link_url );
        this.evento.labelUrl  = done.data.link_label;
        this.evento.mapa      = done.data.ubicacion;
        this.evento.whatsapp  = this.sanitazer.bypassSecurityTrustResourceUrl(done.data.whatsapp);
        this.evento.hashtag   = done.data.hashtag;
        this.evento.album     = done.data.albun;
        this.bullets          = done.data.puntos;
        this.SApi.EventoQueryComprobeInvitation( parseInt( this.ids ), parseInt(this.employeId) ).subscribe( (isMe:any) => {

          if(isMe.error == null){
            this.evento.esInvitado = isMe.data.soyinvitado;
          } else {
            this.evento.isInvitado = null;
          }

        } );

        this.SApi.ItinerarioQueryByEvento( parseInt(this.ids) ).subscribe( (itini:any) => {

          if(itini.error == null){
            this.evento.itinerario = itini.data;
          } else {
            this.evento.itinerario = null;
          }

        } );
      }

    });
  }

  openmap(url:string){
    window.open(url, '_blank');
  }

  toItin($element): void {
    window.location.hash = "#itin";
    window.location.hash = null;
  }

  async openDetailItin(data:any){
    const modal = this.modalCtrl.create({
      component: ViewTaskitinerarioPage,
      componentProps: data
    });

    return (await modal).present();
  }

  async openDetailsTask(num:number, data:any){
    var bullet :any = {};
    switch(num){
      case 1:
        bullet.image  = data.img1;
        bullet.titulo = data.titulo_1;
        bullet.text   = data.text1;
        break;
      case 2:
        bullet.image  = data.img2;
        bullet.titulo = data.titulo_2;
        bullet.text   = data.text2;
        break;
      case 3:
        bullet.image  = data.img3;
        bullet.titulo = data.titulo_3;
        bullet.text   = data.text3;
        break;
      case 4:
        bullet.image  = data.img4;
        bullet.titulo = data.titulo_4;
        bullet.text   = data.text4;
        break;
      case 5:
        bullet.image  = data.img5;
        bullet.titulo = data.titulo_5;
        bullet.text   = data.text5;
        break;
      case 6:
        bullet.image  = data.img6;
        bullet.titulo = data.titulo_6;
        bullet.text   = data.text6;
        break;
    }
    const modal = this.modalCtrl.create({
      component: ViewTaskeventPage,
      componentProps: bullet
    });
    return (await modal).present();
  }

  doRefresh(event){

    setTimeout(() => {
      this.ngOnInit();
      event.target.complete();
    }, 2000);
  
   }

}

