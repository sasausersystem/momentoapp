import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetaileventoPageRoutingModule } from './detailevento-routing.module';

import { DetaileventoPage } from './detailevento.page';
import { SasagaleryComponent } from '../galleryforgoogle/sasagalery/sasagalery.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetaileventoPageRoutingModule
  ],
  declarations: [
    DetaileventoPage,
    SasagaleryComponent
  ]
})
export class DetaileventoPageModule {}
