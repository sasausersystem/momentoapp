import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetaileventoPage } from './detailevento.page';

const routes: Routes = [
  {
    path: '',
    component: DetaileventoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetaileventoPageRoutingModule {}
