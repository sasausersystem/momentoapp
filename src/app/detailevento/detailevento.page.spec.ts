import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetaileventoPage } from './detailevento.page';

describe('DetaileventoPage', () => {
  let component: DetaileventoPage;
  let fixture: ComponentFixture<DetaileventoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetaileventoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetaileventoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
