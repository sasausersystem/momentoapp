import { TestBed } from '@angular/core/testing';

import { GooglealbumapiService } from './googlealbumapi.service';

describe('GooglealbumapiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GooglealbumapiService = TestBed.get(GooglealbumapiService);
    expect(service).toBeTruthy();
  });
});
