import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GooglealbumapiService } from './googlealbumapi.service';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule
  ],
  providers: [
    GooglealbumapiService
  ]
})
export class GalleryforgoogleModule { }
