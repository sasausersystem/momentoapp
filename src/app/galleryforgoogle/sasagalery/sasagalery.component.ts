import { Component, OnInit, AfterViewInit, AfterViewChecked, Input, ElementRef, ViewChild, ViewChildren, AfterContentInit, AfterContentChecked,  OnDestroy, Renderer, ViewContainerRef, TemplateRef } from '@angular/core';
import { ModalController, AngularDelegate } from '@ionic/angular';
import { GooglealbumapiService } from '../googlealbumapi.service';
import { element } from 'protractor';

@Component({
  selector: 'sasagalery',
  templateUrl: './sasagalery.component.html',
  styleUrls: ['./sasagalery.component.scss'],
})
export class SasagaleryComponent implements OnInit, AfterViewInit, AfterViewChecked {

  @Input('album') url : string;

  @ViewChildren('columnPhoto') cajas : ElementRef<any>;

  @ViewChild('modalsita', {static: false}) modalsita : ElementRef<any>;

  imagenes : any = [];
  error    : any;

  constructor(
    private GoogleFecth: GooglealbumapiService,
    private renderer: Renderer, 
    private elem: ElementRef
  ) {
    //this.GoogleAsync();
  }

  GoogleAsync(){
    this.GoogleFecth.GoogleAlbumFetchByUrl(this.url).then((done:any)=>{
      if(done.error == null){
        this.error = null;
        this.imagenes = done.data;
      } else {
        this.error = 1;
      }
    });
  }

  GoogleCloseModal(){
    const modal  : any = this.modalsita;
    modal.nativeElement.style.display = 'none';
  }

  ngOnInit(){
    this.GoogleAsync();
  }

  ngAfterViewInit(){
    this.GoogleAsync();
  }

  ngAfterViewChecked(){

    const cajita : any = this.cajas;
    const modal  : any = this.modalsita;
    if(cajita._results.length > 0){
      const imagenes : [] = cajita._results;
      let   iterator = 0;
      imagenes.forEach((element:any) => {
        element.nativeElement.id = "sasa_gallery_item"+iterator;
        element.nativeElement.setAttribute('data-sasa-gallery-item', iterator);
        element.nativeElement.onclick = function(){
          modal.nativeElement.children[1].src = element.nativeElement.children[0].src;
          modal.nativeElement.style.display = 'block';
        }
        iterator++;
      })
    }
    
    
  }

}

