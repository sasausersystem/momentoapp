import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SasagaleryComponent } from './sasagalery.component';

describe('SasagaleryComponent', () => {
  let component: SasagaleryComponent;
  let fixture: ComponentFixture<SasagaleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SasagaleryComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SasagaleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
