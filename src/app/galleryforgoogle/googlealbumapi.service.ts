import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { element } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class GooglealbumapiService {

  private URIBASE = 'https://sasa.social/api/index.php';

  constructor(
    private httpClient: HttpClient
  ) { 
  }


  GoogleAlbumFetchByUrl(url:string){
    return new Promise( (resolve, reject) => {

      var data   = JSON.stringify({
        album: url
      });      

      this.httpClient.post(this.URIBASE+'?m=google&a=albumfecth', data).subscribe((done) => {
        resolve(done);
      }, (err) => {
        reject(err);
      });

    });
  }
}
