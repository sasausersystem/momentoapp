import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewPortfolioDetailPageRoutingModule } from './view-portfolio-detail-routing.module';

import { ViewPortfolioDetailPage } from './view-portfolio-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewPortfolioDetailPageRoutingModule
  ],
  declarations: [ViewPortfolioDetailPage]
})
export class ViewPortfolioDetailPageModule {}
