import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewPortfolioDetailPage } from './view-portfolio-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ViewPortfolioDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewPortfolioDetailPageRoutingModule {}
