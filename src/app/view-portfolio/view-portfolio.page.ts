import { Component, OnInit } from '@angular/core';
import { ApiRestService } from '../services/apirest.service';
import { ModalController, LoadingController } from '@ionic/angular';
import { ViewPortfolioDetailPage } from '../view-portfolio-detail/view-portfolio-detail.page';
import { NavController, NavParams } from '@ionic/angular';
import { HelperGlobalService } from '../helper-global.service';
import { AnimacionesService } from '../services/animaciones.service';

@Component({
  selector: 'app-view-portfolio',
  templateUrl: './view-portfolio.page.html',
  styleUrls: ['./view-portfolio.page.scss'],
})
export class ViewPortfolioPage implements OnInit {

  datos: any = [];
  error: number = 0;

  constructor(private SApi: ApiRestService, public modalCtrl: ModalController, public loadingCtrl: LoadingController,private navCtrl: NavController, public helpe: HelperGlobalService, private anima: AnimacionesService) { }

  ngOnInit() {
    this.presentLoader();
    this.SApi.SiteQueryPortfolio().subscribe( (portfolio:any) => {
      if(portfolio.error == null){

        this.error = 0;

        this.datos = portfolio.data;

      } else {
        this.datos = [];
        this.error = 1;

      }
    } );
  }

  goToIndex(){
    this.anima.transitionWithSlide('right', 400);
    this.navCtrl.navigateRoot('/');
  }

  async openDetail(data:any){
    const modal = this.modalCtrl.create({
      component: ViewPortfolioDetailPage,
      componentProps: data
    });

    return (await modal).present();
  }

  async presentLoader(){
    const loading = await this.loadingCtrl.create({
      message: 'Cargando',
      duration: 5000
    });
    await loading.present();

    await loading.dismiss();
  }

  doRefresh(event){

    setTimeout(() => {
      this.ngOnInit();
      event.target.complete();
    }, 2000);
  
   }

}
