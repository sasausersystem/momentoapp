import { Component, OnInit } from '@angular/core';
import { HelperGlobalService } from '../helper-global.service';
import { ApiRestService } from '../services/apirest.service';
import { LoadingController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  public aboutInfo : any = {};
  public error     : number = 0;

  constructor(private SApi: ApiRestService, 
              public helper: HelperGlobalService,
              public loadingCtrl: LoadingController,
              private sanitazer: DomSanitizer) { }

  ngOnInit() {
    this.presentLoader();
    this.SApi.SiteQueryAbout().subscribe( (data:any) => {
      if(data.error == null){
        this.aboutInfo = data.data;
        this.error = 0;
        var html = document.getElementById("introtext");
        let videoYT = 'https://www.youtube.com/embed/'+data.data.video+'?rel=0&controls=0&showinfo=0';
        html.innerHTML = data.data.intro;
        this.aboutInfo.image = this.sanitazer.bypassSecurityTrustResourceUrl(data.data.image);
        this.aboutInfo.video = this.sanitazer.bypassSecurityTrustResourceUrl(videoYT);
      } else {
        this.error = 1;
      }
    } );
  }

  doRefresh(event){

    setTimeout(() => {
      this.ngOnInit();
      event.target.complete();
    }, 2000);
  
  }

  async presentLoader(){
    const loading = await this.loadingCtrl.create({
      message: 'Cargando',
      duration: 5000
    });
    await loading.present();

    await loading.dismiss();
  }

}
